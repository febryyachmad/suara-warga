<?php include('change_form.php') ?>

<?php startblock('change_form')?>
	<form method="POST" action="<?= base_url() ?><?= $action ?>" id="change_form">
		<input type="hidden" name="id" value="<?= $_id ?>">
		<div class="card">
			<div class="card-body">
				<h2 class="card-title">Edit <?= $title ?></h2>

				<div class="form-group">
					<label>Nama</label>
					<input type="text" class="form-control" placeholder="" name="nama">
					<i class="form-group__bar"></i>
				</div>

				<div class="form-group">
					<label>Tempat Lahir</label>
					<input type="text" class="form-control" placeholder="" name="tempat_lahir">
					<i class="form-group__bar"></i>
				</div>

				<div class="form-group">
					<label>Tanggal Lahir</label>
					<input type="text" class="form-control datepicker" placeholder="" name="tanggal_lahir">
					<i class="form-group__bar"></i>
				</div>

				<div class="form-group">
					<label>Jenis Kelamin</label>
					<div class="custom-control custom-radio">
                        <input type="radio" name="jenis_kelamin" class="custom-control-input" value="L">
                        <label class="custom-control-label" for="customRadio1">Laki-Laki</label>
                    </div>
                    <div class="clearfix mb-2"></div>
                    <div class="custom-control custom-radio">
                        <input type="radio" name="jenis_kelamin" class="custom-control-input" value="P">
                        <label class="custom-control-label" for="customRadio1">Perempuan</label>
                    </div>
                    <div class="clearfix mb-2"></div>
				</div>

				<div class="form-group">
					<label>Email</label>
					<input type="text" class="form-control" placeholder="" name="email">
					<i class="form-group__bar"></i>
				</div>

				<div class="form-group">
					<label>Password</label>
					<input type="text" class="form-control" placeholder="" name="password">
					<i class="form-group__bar"></i>
				</div>

			</div>
			<div class="card-footer">
				<button type="button" class="btn btn-success button-submit">Simpan</button>
				<a href="<?= base_url() ?>admin" type="button" class="btn btn-danger">Batal</a>
			</div>
		</div>
	</form>
<?php endblock() ?>

<?php startblock("custom_js") ?>
	<?php superblock() ?>
	<script type="text/javascript">
		$.ajax({
			url: "<?= base_url() ?>admin/akun/show/?id=<?php echo $_id?>",
			type: "GET",
			success: function(respon){
				respon = JSON.parse(respon);
				console.log(respon)
				if(respon){
					$("input[name='nik']").val(respon.nik)
					$("input[name='password']").val(respon.password)
					$("input[name='nama']").val(respon.nama)
					$("input[name='tempat_lahir']").val(respon.tempat_lahir)
					$("input[name='tanggal_lahir']").val(respon.tanggal_lahir)
					$("input[type='radio'][value='"+respon.jenis_kelamin+"']").prop("checked", true)
					$("input[name='email']").val(respon.email)
					$("select[name='grup_id']").val(respon.grup_id).trigger("change")
					// if(respon.keterangan != null && respon.keterangan != ""){
					// 	$("textarea[name='keterangan']").val(respon.keterangan)
					// }
				}
			}
		})
	</script>
<?php endblock() ?>