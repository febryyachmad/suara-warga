<ul class="navigation">
    <li class="<?php if(isset($menu_active) && $menu_active == 'home') echo "navigation__active" ?>">
        <a href="<?= base_url() ?>admin"><i class="zmdi zmdi-home"></i> Home</a>
    </li>

    <li class="<?php if(isset($_GET['p']) && $_GET['p'] == 'semua-pengaduan') echo "navigation__active" ?>">
        <a href="<?= base_url() ?>admin/pengaduan?p=semua-pengaduan"><i class="zmdi zmdi-assignment"></i> Semua Pengaduan</a>
    </li>

    <li class="<?php if(isset($_GET['p']) && $_GET['p'] == 'sudah-tertangani') echo "navigation__active" ?>">
        <a href="<?= base_url() ?>admin/pengaduan?p=sudah-tertangani"><i class="zmdi zmdi-assignment-check"></i> Data Sudah Tertangani</a>
    </li>

    <li class="<?php if(isset($_GET['p']) && $_GET['p'] == 'verifikasi') echo "navigation__active" ?>">
        <a href="<?= base_url() ?>admin/pengaduan?p=verifikasi"><i class="zmdi zmdi-assignment-o"></i>Verifikasi</a>
    </li>

    <li class="<?php if(isset($_GET['p']) && $_GET['p'] == 'belum-tertangani') echo "navigation__active" ?>">
        <a href="<?= base_url() ?>admin/pengaduan?p=belum-tertangani"><i class="zmdi zmdi-assignment-o"></i>Pengaduan Belum Tertangani</a>
    </li>

    <li class="<?php if(isset($_GET['p']) && $_GET['p'] == 'archive') echo "navigation__active" ?>">
        <a href="<?= base_url() ?>admin/pengaduan?p=archive"><i class="zmdi zmdi-assignment-o"></i>Pengaduan Archive</a>
    </li>

    <li class="<?php if(isset($menu_active) && $menu_active == 'statistik') echo "navigation__active" ?>">
        <a href="<?= base_url() ?>admin/statistik"><i class="zmdi zmdi-chart"></i>Statistik</a>
    </li> 

    <?php if($user_saya->grup_id == 8) { ?>

    <?php $menu_akun = array('akun', 'grup') ?>
    <li class="navigation__sub <?php if(isset($menu_active) && in_array($menu_active, $menu_akun)) echo "navigation__sub--active navigation__sub--toggled" ?>">
        <a href="#"><i class="zmdi zmdi-accounts"></i> Akun</a>

        <ul>
            <li class="<?php if(isset($menu_active) && $menu_active == 'akun') echo "navigation__active" ?>">
                <a href="<?= base_url() ?>admin/akun">Akun</a>
            </li>

            <li class="<?php if(isset($menu_active) && $menu_active == 'grup') echo "navigation__active" ?>">
                <a href="<?= base_url() ?>admin/grup">Grup</a>
            </li>
        </ul>
    </li>


    <?php $menu_master = array('desa', 'jenispengaduan') ?>
    <li class="navigation__sub <?php if(isset($menu_active) && in_array($menu_active, $menu_master)) echo "navigation__sub--active navigation__sub--toggled" ?>">
        <a href="#"><i class="zmdi zmdi-view-week"></i> Master</a>

        <ul>
            <li class="<?php if(isset($menu_active) && $menu_active == 'desa') echo "navigation__active" ?>">
                <a href="<?= base_url() ?>admin/desa">Desa</a>
            </li>

            <li class="<?php if(isset($menu_active) && $menu_active == 'jenispengaduan') echo "navigation__active" ?>">
                <a href="<?= base_url() ?>admin/jenispengaduan">Jenis Pengaduan</a>
            </li>
        </ul>
    </li>
    <?php } ?>

</ul>