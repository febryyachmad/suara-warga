<?php include('base.php') ?>

<?php startblock('title') ?>
	<?= $title ?>
<?php endblock() ?>

<?php startblock('css') ?>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>static/admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>static/admin/vendors/bower_components/select2/dist/css/select2.min.css">
<?php endblock() ?>

<?php startblock('js') ?>
	<script type="text/javascript" src="<?= base_url() ?>static/admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>static/admin/vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<?php endblock() ?>

<?php startblock('custom_css') ?>
	<style type="text/css">
		.form-control{
			border: 1px solid #b7b6b6;
		}
	</style>
<?php endblock() ?>

<?php startblock('header') ?>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?= base_url() ?>admin">Home</a></li>
		<li class="breadcrumb-item">Master</li>
		<li class="breadcrumb-item active"><?= $title ?></li>
	</ol>
<?php endblock() ?>

<?php startblock('isi') ?>
	<!-- <div class="card">
		<div class="card-header">
			<h2 class="card-title">Pencarian</h2>
		</div>
		<div class="card-body">
			<div class="form-group">
	            <input type="text" class="form-control" placeholder="Cari...">
	            <i class="form-group__bar"></i>
	        </div>
		</div>
		<div class="card-footer">
			<button type="button" class="btn btn-success">Cari</button>
		</div>
	</div> -->

	<div class="toolbar">
        <div class="toolbar__label"><span class="hidden-xs-down">Total</span> <span class="change_list__total"></span> Records</div>

        <div class="actions">
            <i class="actions__item zmdi zmdi-search" data-ma-action="toolbar-search-open"></i>
            <!-- <a href="#" class="actions__item zmdi zmdi-time"></a>
            <div class="dropdown actions__item">
                <i class="zmdi zmdi-sort" data-toggle="dropdown"></i>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item">Last Modified</a>
                    <a href="#" class="dropdown-item">Name</a>
                    <a href="#" class="dropdown-item">Size</a>
                </div>
            </div> -->
            <a href="#" class="actions__item zmdi zmdi-help-outline"></a>
        </div>

        <div class="toolbar__search">
            <input type="text" placeholder="Search..." class="change_list__search" value="">
            <i class="toolbar__search__close zmdi zmdi-long-arrow-left" data-ma-action="toolbar-search-close" onclick="pagination_(0), search_list('')"></i>
        </div>
    </div>

	<div class="card">
		<div class="card-body">
			<h2 class="card-title">Daftar <?= $title ?></h2>
			<div class="actions">
				<?php startblock('change_list__action') ?>
					<a href="<?= base_url().$url ?>">
						<button type="button" class="btn btn-success">Tambah</button>
					</a>
				<?php endblock() ?>
			</div>
			<!-- <div class="table-responsive"> -->
				<?php emptyblock('change_list')?>
			<!-- </div> -->
		</div>
	</div>
<?php endblock() ?>


<?php startblock('isi1') ?>
	<!-- <div class="card">
		<div class="card-header">
			<h2 class="card-title">Pencarian</h2>
		</div>
		<div class="card-body">
			<div class="form-group">
	            <input type="text" class="form-control" placeholder="Cari...">
	            <i class="form-group__bar"></i>
	        </div>
		</div>
		<div class="card-footer">
			<button type="button" class="btn btn-success">Cari</button>
		</div>
	</div> -->

	<div class="toolbar">
        <div class="toolbar__label"><span class="hidden-xs-down">Total</span> <span class="change_list__total"></span> Records</div>

        <div class="actions">
            <i class="actions__item zmdi zmdi-search" data-ma-action="toolbar-search-open"></i>
            <!-- <a href="#" class="actions__item zmdi zmdi-time"></a>
            <div class="dropdown actions__item">
                <i class="zmdi zmdi-sort" data-toggle="dropdown"></i>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item">Last Modified</a>
                    <a href="#" class="dropdown-item">Name</a>
                    <a href="#" class="dropdown-item">Size</a>
                </div>
            </div> -->
            <a href="#" class="actions__item zmdi zmdi-help-outline"></a>
        </div>

        <div class="toolbar__search">
            <input type="text" placeholder="Search..." class="change_list__search" value="">
            <i class="toolbar__search__close zmdi zmdi-long-arrow-left" data-ma-action="toolbar-search-close" onclick="pagination_(0), search_list('')"></i>
        </div>
    </div>

    <div class="card1">
		<div class="card-body">
			<h2 class="card-title">Data <?= $title ?></h2>
			<div class="table-responsive">
				<?php emptyblock('change_list1')?>
			</div>
		</div>
	</div>	
<?php endblock() ?>



<?php startblock('custom_js') ?>
	<script type="text/javascript">
		$(document).ready(function(){
			pagination_(getUrlParameter('page'))
			$('.change_list__search').val(getUrlParameter('q'));
		});

		this_page = 0
		if(getUrlParameter('page') == undefined){
			changeUrl("q", "")
			changeUrl("page", this_page)
		}

		function search_list(q){
			key = q.replace(/\s+/g, '-')
			changeUrl("q", key)
			pagination_(0)
		}

		$('.change_list__search').keypress(function (e) {
			search_list($(this).val())
		})

		function delete_(elemen){
			swal({
				title: 'Apakah Anda yakin?',
				text: "Data tidak akan bisa kembali jika sudah terhapus!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#68C39F',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya, Hapus data!'
			}).then((result) => {
				if (result) {
					$.ajax({
						url: elemen.data('url'),
						type: 'GET',
						success: function(respon){
							respon = JSON.parse(respon)
							if(respon.success == true){
								swal({
									title: 'Terhapus!',
									text: 'Data berhasil terhapus.',
									type: 'success',
									showConfirmButton: false,
									timer: 1500,
								})
							}else{
								swal({
									title: 'Error!',
									text: 'Terjadi kesalahan saat proses menghapus data.',
									type: 'error',
									showConfirmButton: false,
									timer: 1500,
								})
							}
							pagination_(getUrlParameter('page'))
						}
					})
				}
			})
		}
	</script>
<?php endblock() ?>