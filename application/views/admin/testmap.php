<div id="googleMap" style="width:100%;height:400px;"></div>

<script>
	function myMap() {
		var mapProp= {
			center:new google.maps.LatLng(51.508742,-0.120850),
			zoom:5,
		};
		var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
	}

	// $(document).ready(function() {
	// 	var mapOptions = {
	// 		zoom: 10,
	// 		center: new google.maps.LatLng(-6.8922897, 112.0422328)
	// 	};
	// 	var map = new google.maps.Map(document.getElementById('map'), mapOptions);
	// 	var marker;
	// 	var locations = [];

	// 	$.ajax({
	// 		type: "GET",
	// 		url: "/admin/fingerprints/mesinfingerprint/get-lintang-bujur-mesin/",
	// 		success: function(respon) {
	// 			respon = JSON.parse(respon)
	// 			for(var i=0; i<respon.length; i++) {
	// 				if(respon[i].lt && respon[i].lg){
	// 					place_marker(respon[i].name, respon[i].jumlah_pegawai, respon[i].lt, respon[i].lg)
	// 				}
	// 			}
	// 		}
	// 	});

	// 	function place_marker(content, jumlah_pegawai, lt, lg){
	// 		var latLng = new google.maps.LatLng( parseFloat(lt), parseFloat(lg));
	// 		var image = {
	// 			url: '/static/images/peta-marker.png',
	// 			// This marker is 20 pixels wide by 32 pixels high.
	// 			size: new google.maps.Size(20, 32),
	// 			// The origin for this image is (0, 0).
	// 			origin: new google.maps.Point(0, 0),
	// 			// The anchor for this image is the base of the flagpole at (0, 32).
	// 			anchor: new google.maps.Point(0, 32)
	// 		};

	// 		var marker = new google.maps.Marker({
	// 			position : latLng,
	// 			map      : map,
	// 			icon: image,
	// 			animation: google.maps.Animation.DROP
	// 		});
	// 		var infowindow = new google.maps.InfoWindow({});

	// 		google.maps.event.addListener(marker, 'click', function(){
	// 			infowindow.close();
	// 			infowindow.setContent( "<div id='infowindow'>"+content+" | "+jumlah_pegawai +" Pegawai</div>");
	// 			infowindow.open(map, marker);
	// 		});
	// 	}
	// });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAY8xmhot-viwVybAxaOD_Qissx0wCJUcA&callback=myMap"></script>