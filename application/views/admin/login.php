<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/css/app.min.css">
    </head>
    <body data-ma-theme="green">
        <form method="POST" action="<?= base_url() ?>admin/login/login" id="change_form" onsubmit="return false">
            <div class="login">
                <div class="login__block active" id="l-login">
                    <div class="login__block__header">
                        <i class="zmdi zmdi-account-circle"></i>
                        Hi there! Please Sign in
                    </div>

                    <div class="login__block__body">
                        <div class="change-form-message"></div>
                        <div class="form-group form-group--float form-group--centered">
                            <input type="text" class="form-control" name="username" required="">
                            <label>NIK/Email</label>
                            <i class="form-group__bar"></i>
                        </div>

                        <div class="form-group form-group--float form-group--centered">
                            <input type="password" class="form-control" name="password" required="">
                            <label>Password</label>
                            <i class="form-group__bar"></i>
                        </div>

                        <button class="btn btn--icon login__block__btn button-submit"><i class="zmdi zmdi-long-arrow-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>static/admin/js/app.min.js"></script>
        <script src="<?= base_url() ?>static/js/jquery.form.min.js"></script>
        <script type="text/javascript">
            $('.button-submit').on('click', function(){
                form = $("#change_form")
                form.ajaxSubmit({
                    url: form.attr("action"),
                    type: "POST",
                    data: form.serialize(),
                    success: function(respon){
                        respon = JSON.parse(respon);
                        if(respon.sukses == true){
                            window.location.href = "<?= base_url() ?>admin";
                        }else{
                            error_html = '<div class="alert alert-danger">'+
                                        '<h4 class="alert-heading">Terjadi Kesalahan</h4>'+
                                        respon.error+
                                        '</div>';
                            $('.change-form-message').html(error_html);
                        }
                    }
                });
            });
        </script>
    </body>
</html>