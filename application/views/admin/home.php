<?php include('base.php') ?>

<?php startblock('title') ?>
	<?= $title ?>
<?php endblock() ?>

<?php startblock('header') ?>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="#">Home</a></li>
	</ol>
<?php endblock() ?>

<?php startblock('isi') ?>
	<div class="row quick-stats">
        <div class="col-sm-6 col-md-3">
            <a href="<?= base_url() ?>admin/pengaduan?p=semua-pengaduan">
                <div class="quick-stats__item bg-blue">
                    <div class="quick-stats__info">
                        <h2><?= $jumlah_pengaduan ?></h2>
                        <small>Total Pengaduan</small>
                    </div>

                    <div class="quick-stats__chart sparkline-bar-stats"><canvas style="display: inline-block; width: 58px; height: 36px; vertical-align: top;" width="58" height="36"></canvas></div>
                </div>
            </a>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="quick-stats__item bg-amber">
                <div class="quick-stats__info">
                    <h2><?= $jumlah_pengaduan_belum ?></h2>
                    <small>Pengaduan Belum Tertangani</small>
                </div>

                <div class="quick-stats__chart sparkline-bar-stats"><canvas style="display: inline-block; width: 58px; height: 36px; vertical-align: top;" width="58" height="36"></canvas></div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="quick-stats__item bg-purple">
                <div class="quick-stats__info">
                    <h2><?= $jumlah_pengaduan_sudah ?></h2>
                    <small>Pengaduan Tertangani</small>
                </div>

                <div class="quick-stats__chart sparkline-bar-stats"><canvas style="display: inline-block; width: 58px; height: 36px; vertical-align: top;" width="58" height="36"></canvas></div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="quick-stats__item bg-red">
                <div class="quick-stats__info">
                    <h2><?= $jumlah_pengaduan_archive ?></h2>
                    <small>Pengaduan Archive</small>
                </div>

                <div class="quick-stats__chart sparkline-bar-stats"><canvas style="display: inline-block; width: 58px; height: 36px; vertical-align: top;" width="58" height="36"></canvas></div>
            </div>
        </div>
    </div>
<?php endblock() ?>