<?php include('change_form.php') ?>

<?php startblock('change_form')?>
	<form method="POST" action="<?= base_url() ?><?= $action ?>" id="change_form">
		<div class="card">
			<div class="card-body">
				<h2 class="card-title">Add <?= $title ?></h2>

				<div class="form-group">
					<label>NIK</label>
					<input type="text" class="form-control" placeholder="" name="nik">
					<i class="form-group__bar"></i>
				</div>

				<div class="form-group">
					<label>Password</label>
					<input type="password" class="form-control" placeholder="" name="password">
					<i class="form-group__bar"></i>
				</div>

				<div class="form-group">
					<label>Nama</label>
					<input type="text" class="form-control" placeholder="" name="nama">
					<i class="form-group__bar"></i>
				</div>

				<div class="form-group">
					<label>Tempat Lahir</label>
					<input type="text" class="form-control" placeholder="" name="tempat_lahir">
					<i class="form-group__bar"></i>
				</div>

				<div class="form-group">
					<label>Tanggal Lahir</label>
					<input type="text" class="form-control datepicker" placeholder="" name="tanggal_lahir">
					<i class="form-group__bar"></i>
				</div>

				<div class="form-group">
					<label>Jenis Kelamin</label>
					<div class="custom-control custom-radio">
                        <input type="radio" name="jenis_kelamin" class="custom-control-input" value="L">
                        <label class="custom-control-label" for="customRadio1">Laki-Laki</label>
                    </div>
                    <div class="clearfix mb-2"></div>
                    <div class="custom-control custom-radio">
                        <input type="radio" name="jenis_kelamin" class="custom-control-input" value="P">
                        <label class="custom-control-label" for="customRadio1">Perempuan</label>
                    </div>
                    <div class="clearfix mb-2"></div>
				</div>

				<div class="form-group">
					<label>Email</label>
					<input type="text" class="form-control" placeholder="" name="email">
					<i class="form-group__bar"></i>
				</div>


				<div class="form-group">
					<label>Grup</label>
					<!-- <input type="tex?t" class="form-control" placeholder="" name="grup_id"> -->
					<!-- <i class="form-group__bar"></i> -->
					<select class="select2" name="grup_id">
						<?php foreach($list_grup as $g) { ?>
							<option value="<?= $g->id; ?>"><?= $g->nama_grup; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="card-footer">
				<button type="button" class="btn btn-success button-submit">Simpan</button>
				<a href="<?= base_url() ?>admin/akun" type="button" class="btn btn-danger">Batal</a>
			</div>
		</div>
	</form>
<?php endblock() ?>