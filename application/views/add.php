<?php include('base.php') ?>

<?php startblock('title') ?>
    Tambah Pengaduan
<?php endblock() ?>

<?php startblock('css') ?>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>static/admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>static/admin/vendors/bower_components/select2/dist/css/select2.min.css">
<?php endblock() ?>

<?php startblock('js') ?>
	<script src="<?= base_url() ?>static/js/jquery.form.min.js"></script>
	<script src="<?= base_url() ?>static/admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
	<script src="<?= base_url() ?>static/admin/vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAY8xmhot-viwVybAxaOD_Qissx0wCJUcA&libraries=places"></script>
    <script src="<?= base_url() ?>static/js/locationpicker.jquery.js"></script>
<?php endblock() ?>

<?php startblock('custom_css') ?>
	<style type="text/css">
		.form-control[type="text"], textarea.form-control{
			border: 1px solid #b7b6b6;
		}

		.select2.select2-container.select2-container--default {
			border: 1px solid #e3e3e3;
			padding-right: 5px;
			padding-left: 5px;
			border-radius: 3px;
		}
	</style>
<?php endblock() ?>	

<?php startblock('content') ?>
	<form action="<?= base_url() ?>addaction" enctype="multipart/form-data" onsubmit="return false" id="change_form">
		<div class="change-form-message"></div>
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Tambah Pengaduan</h4>
				<div class="form-group">
					<label>Jenis Pengaduan</label>
					<select class="select2" name="jenispengaduan_id">
						<option> -- Pilih Jenis Pengaduan -- </option>
						<?php foreach($list_jenispengaduan as $g) { ?>
							<option value="<?= $g->id; ?>"><?= $g->nama_jenis; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-group">
					<label>Isi Pengaduan</label>
					<textarea class="form-control" name="isi_pengaduan" rows="5"></textarea>
					<i class="form-group__bar"></i>
				</div>
				<div class="form-group">
					<label>Desa Lokasi</label>
					<select class="select2" name="desa_id">
						<option> -- Pilih Desa -- </option>
						<?php foreach($list_desa as $g) { ?>
							<option value="<?= $g->id; ?>"><?= $g->nama_desa; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-group">
					<label>Alamat Lokasi</label>
					<input type="text" class="form-control" placeholder="" name="alamat_lokasi">
					<i class="form-group__bar"></i>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-5">
							<label>Lintang</label>
							<input type="text" class="form-control" placeholder="" name="lg" id="id_lintang">
						</div>
						<div class="col-md-5">
							<label>Bujur</label>
							<input type="text" class="form-control" placeholder="" name="lt" id="id_bujur">
						</div>
						<div class="col-md-2" style="margin-top: 25px;">
							<button class="btn btn-primary btn--icon-text get_gps"><i class="zmdi zmdi-my-location"></i> GPS</button>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Photo</label>
					<input type="file" class="form-control" placeholder="" name="image_file">
					<span>Gunakan file berextension jpg, jpeg, png. Max size file 3 MB	</span>
				</div>
			</div>
			<div class="card-footer">
				<button type="button" class="btn btn-success button-submit">Simpan</button>
				<a href="<?= base_url() ?>" type="button" class="btn btn-danger">Batal</a>
			</div>
		</div>
	</form>

	<div class="modal fade" id="modal-default" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">Ambil Lokasi GPS</h5>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                    	<div class="form-group">
                    		<label class="col-sm-2 control-label">Location:</label>
                    		<div class="col-sm-10">
                    			<input type="text" id="us11-address" class="form-control" placeholder="Enter a location" autocomplete="off" style="max-width: none;">
                    		</div>
                    	</div>
                    	<div class="form-group">
                    		<label class="col-sm-2 control-label">Radius:</label>
                    		<div class="col-sm-5">
                    			<input type="text" id="us11-radius" class="form-control">
                    		</div>
                    	</div>
                    	<div class="somegoogle" style="width:100%; height: 400px;"></div>
                    	<div class="clearfix">&nbsp;</div>
                    	<div class="m-t-small">
                    		<label class="p-r-small col-sm-1 control-label">Latitude:</label>
                    		<div class="col-sm-5">
                    			<input type="text" id="us11-lat" style="width: 100%" class="form-control">
                    		</div>
                    		<label class="p-r-small col-sm-2 control-label">Longitude:</label>
                    		<div class="col-sm-5">
                    			<input type="text" id="us11-lon" style="width: 100%" class="form-control">
                    		</div>
                    	</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Pilih</button>
                </div>
            </div>
        </div>
    </div>
<?php endblock() ?>

<?php startblock('custom_js') ?>
	<script type="text/javascript">
		var lat = -7.844457
		var long = 112.0041685 
		navigator.geolocation.getCurrentPosition(function(location) {
			var lat = location.coords.latitude
			var long = location.coords.longitude
			$('#id_lintang').val(lat)
			$('#id_bujur').val(long)
		});

		$('.get_gps').on('click', function(){
			// var lat = -7.844457
			// var long = 112.0041685

			$('#modal-default').modal('show')
			$('.somegoogle').locationpicker('autosize');
			
			if($('#id_lintang').val() != "" && $('#id_bujur').val() != ""){
				$('#us11-lat').val($('#id_lintang').val())
				$('#us11-lon').val($('#id_bujur').val())
			}
			if($('#us11-lat').val() != "" && $('#us11-lon').val() != ""){
				lat = $('#us11-lat').val()
				long = $('#us11-lon').val()
			}

			$('.somegoogle').locationpicker({
				location: {
					latitude: lat,
					longitude: long
				},
				radius: 300,
				inputBinding: {
					latitudeInput: $('#us11-lat'),
					longitudeInput: $('#us11-lon'),
					radiusInput: $('#us11-radius'),
					locationNameInput: $('#us11-address')
				},
				enableAutocomplete: true,
				onchanged: function (currentLocation, radius, isMarkerDropped) {
			        // alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
			        $('#id_lintang').val(currentLocation.latitude);
			        $('#id_bujur').val(currentLocation.longitude);
			    }
			});
		});

		var button_loading = function(element, type){
			if(type == "disabled"){
				text_ori = element.html()
				element.attr("data-text", text_ori)
				element.attr("disabled", true)
				element.text("Loading")
			}else{
				element.attr("disabled", false)
				element.html(element.attr("data-text"))
			}
		}

		$('.button-submit').on('click', function(){
			form = $("#change_form")
			$this = $(this)
			button_loading($this, "disabled")
			form.ajaxSubmit({
				url: form.attr("action"),
				type: "POST",
				data: form.serialize(),
				success: function(respon){
					button_loading($this, "show")
					respon = JSON.parse(respon)
					if(respon.success == false){
						error_html = '<div class="alert alert-danger" role="alert">'+
									'<h4 class="alert-heading">Error!</h4>'+
									respon.error+
									'</div>';
						$('.change-form-message').html(error_html);
						window.scrollTo(0,0)
					}else{
						swal({
							title: 'Berhasil!',
							text: 'Data berhasil tersimpan.',
							type: 'success',
							showConfirmButton: false,
							timer: 1500,
						});
						setTimeout(function () {
							window.location.href = "<?= base_url() ?>";
						},2000);
					}
				},
				error: function(){
					button_loading($this, "show")
					console.log("error")
				}
			})
		});
	</script>
<?php endblock() ?>