<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_akun extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	public function add(){
		$field = array(
			'nik' => $this->input->post('nik'),
			'password' => md5($this->input->post('password')),
			'nama' => $this->input->post('nama'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'tanggal_lahir' => $this->input->post('tanggal_lahir'),
			'email' => $this->input->post('email'),
			'grup_id' => $this->input->post('grup_id'),
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			);
		$this->db->insert('akun', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function addapi(){
		$field = array(
			'nik' => $this->input->post('nik'),
			'password' => md5($this->input->post('password')),
			'nama' => $this->input->post('nama'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'tanggal_lahir' => $this->input->post('tanggal_lahir'),
			'email' => $this->input->post('email'),
			'grup_id' => 4,
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			);
		$this->db->insert('akun', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function update(){
		$id = $this->input->post('id');
		$field = array(
			'nik'=>$this->input->post('nik'),
			'nama'=>$this->input->post('nama'),
			'tempat_lahir'=>$this->input->post('tempat_lahir'),
			'tanggal_lahir'=>$this->input->post('tanggal_lahir'),
			'jenis_kelamin'=>$this->input->post('jenis_kelamin'),
			'email'=>$this->input->post('email'),
			'grup_id'=>$this->input->post('grup_id'),
			);
		$this->db->where('id', $id);
		$this->db->update('akun', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function update_password(){
		$field = array(
			'password' => md5($this->input->post('password'))
			);
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('akun', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function delete(){
        $id = $this->input->get('id');
        $this->db->where('id', $id);
        $this->db->delete('akun');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

	function getlist($limit, $start, $st = NULL, $id){
		if ($st == NULL) $st = "";
		if(!$limit){
			$limit = 0;
		}
		if(!$start){
			$start = 0;
		}
		$limit = $limit;

		if($start > 0){
			$limit = $start.",".$limit;
		}
		
		$this->db->select('akun.id, akun.nik, akun.nama, akun.tempat_lahir, akun.tanggal_lahir, akun.jenis_kelamin, akun.email, grup.nama_grup, akun.grup_id');
		$this->db->from('akun');
		$this->db->join('grup', 'akun.grup_id=grup.id', 'left');
		$this->db->limit($limit, $start);

		if($st){
			$st = strtolower($st);
			$st = str_replace('-', ' ', $st);
			// untuk mancari berdasarkan field yang di inginkan
			$this->db->like('lower(akun.nama)', $st, 'both');
			$this->db->or_like('lower(akun.nik)', $st, 'both');
			// $this->db->like('akun.nik', $st);
		}

		if($id != NULL){
			$this->db->where('akun.id', $id);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function getAll(){
		$this->db->select('*');
		$this->db->from('akun');
		$query = $this->db->get();
		return $query->result();
	}

	function get_count($st = NULL){
		if ($st == NULL) $st = "";
		$sql = "select * from akun";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function check_unique_user_email($id = '', $email) {
        $this->db->where('email', $email);
        if($id) {
            $this->db->where_not_in('id', $id);
        }
        return $this->db->get('akun')->num_rows();
    }

    function check_unique_user_nik($id = '', $nik) {
        $this->db->where('nik', $nik);
        if($id) {
            $this->db->where_not_in('id', $id);
        }
        return $this->db->get('akun')->num_rows();
    }

    // untuk mengecek login akun api
    function cek_api(){
    	$username = NULL;
		$api_key = NULL;
    	if(isset($_REQUEST['username'])) $username = $_REQUEST['username'];
		if(isset($_REQUEST['api_key'])) $api_key = $_REQUEST['api_key'];

		$this->db->where('email', $username);
		$this->db->or_where('nik', $username);
		$this->db->where('api_key', $api_key);
		$data = $this->db->get('akun');
		// print_r($data);
		if($data->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}