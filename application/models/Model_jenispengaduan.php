<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_jenispengaduan extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	public function add(){
		$field = array(
			'nama_jenis'=>$this->input->post('nama_jenis'),
			'keterangan_jenis'=>$this->input->post('keterangan_jenis'),
			);
		$this->db->insert('jenis_pengaduan', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function update(){
		$id = $this->input->post('id');
		$field = array(
			'nama_jenis'=>$this->input->post('nama_jenis'),
			'keterangan_jenis'=>$this->input->post('keterangan_jenis'),
			);
		$this->db->where('id', $id);
		$this->db->update('jenis_pengaduan', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function delete(){
        $id = $this->input->get('id');
        $this->db->where('id', $id);
        $this->db->delete('jenis_pengaduan');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

	function getlist($limit, $start, $st = NULL, $id){
		if ($st == NULL) $st = "";
		if(!$limit){
			$limit = 0;
		}
		if(!$start){
			$start = 0;
		}
		$limit = $limit;

		if($start > 0){
			$limit = $start.",".$limit;
		}
		
		$this->db->select('*');
		$this->db->from('jenis_pengaduan');
		$this->db->limit($limit, $start);
		if($id != NULL){
			$this->db->where('id', $id);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function getAll(){
		$this->db->select('*');
		$this->db->from('jenis_pengaduan');
		$query = $this->db->get();
		return $query->result();
	}

	function get_count($st = NULL){
		if ($st == NULL) $st = "";
		$sql = "select * from jenis_pengaduan";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
}