<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_grup extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	public function add(){
		$field = array(
			'nama_grup'=>$this->input->post('nama_grup'),
			'keterangan'=>$this->input->post('keterangan'),
			);
		$this->db->insert('grup', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function update(){
		$id = $this->input->post('id');
		$field = array(
			'nama_grup'=>$this->input->post('nama_grup'),
			'keterangan'=>$this->input->post('keterangan'),
			);
		$this->db->where('id', $id);
		$this->db->update('grup', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function delete(){
        $id = $this->input->get('id');
        $this->db->where('id', $id);
        $this->db->delete('grup');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

	function getlist($limit, $start, $st = NULL, $id){
		if ($st == NULL) $st = "";
		if(!$limit){
			$limit = 0;
		}
		if(!$start){
			$start = 0;
		}
		$limit = $limit;

		if($start > 0){
			$limit = $start.",".$limit;
		}
		
		$this->db->select('*');
		$this->db->from('grup');
		$this->db->limit($limit, $start);
		if($id != NULL){
			$this->db->where('id', $id);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function getAll(){
		$this->db->select('*');
		$this->db->from('grup');
		$query = $this->db->get();
		return $query->result();
	}

	function get_count($st = NULL){
		if ($st == NULL) $st = "";
		$sql = "select * from grup";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
}