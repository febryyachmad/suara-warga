<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Model_jenispengaduan');
		$this->load->model('Model_pengaduan');
		$this->load->model('Model_desa');
		$this->load->model('Model_akun');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->load->view('beranda');
	}

	public function show(){
		$limit = 20;
		$start = 0;
		$next = 20;
		$previous = 0;
		$q = "";
		$id = NULL;
		$p = NULL;

		if(isset($_REQUEST['limit'])) $limit = $_REQUEST['limit'];
		if(isset($_REQUEST['start'])) $start = $_REQUEST['start'];
		if(isset($_REQUEST['q'])) $q = $_REQUEST['q'];
		if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
		if(isset($_REQUEST['p'])) $p = $_REQUEST['p'];

		$result = $this->Model_pengaduan->getlist($limit, $start, $q, $id, $p);
		$total_count_page = count($result);

		$total_count = $this->Model_pengaduan->get_count($q);
		$total_pagination = $total_count / $limit;
		$total_pagination = (int)$total_pagination;
		if ($total_count % $limit > 0){
			$total_pagination = $total_pagination + 1;
		}
		$link_next = "";
		if(count($result) == $limit){
			$link_next = "/admin/show?limit=".$limit."&start=".$next;
		}

		$link_prev = "";
		if($start != 0){
			$link_prev = "/admin/show?limit=".$limit."&start=".$previous;
		}

		if((int)$start != 0){
			$next = (int)$start+20;
			$previous = (int)$start-20;
		}
		$meta = array(
			'start' => $start,
			'limit' => $limit,
			'next' => $link_next,
			'previous' => $link_prev,
			'total_count' => $total_count,
			'total_pagination' => $total_pagination,
			'total_count_page' => $total_count_page,
		);

		$data = array(
			'meta' => $meta,
			'objects' => $result,
		);
		if(isset($_REQUEST['id'])){
			$data = $result[0];
		}
		echo json_encode($data);
	}

	public function login(){
		if($this->session->userdata('frontend_logged_in')){
			redirect(base_url(), "refresh");
		}
		$this->load->view('login');
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url(), "refresh");
	}

	function actionlogin(){
		$data['sukses'] = false;
		$this->form_validation->set_rules('username','NIK/Email','required');
		$this->form_validation->set_rules('password','Password','required');
		if($this->form_validation->run() != false){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$this->db->where('nik', $username);
			$this->db->or_where('email', $username);
			$this->db->where('password', md5($password));
			$query = $this->db->get('akun');
			if($query->num_rows() > 0){
				$result = $query->result();
				$result = $result[0];
				$data['sukses'] = true;			
				$data['result'] = $result;
				unset($result->password); // untuk membuang field password dari array
				$field = array(
					'frontend_logged_in' => TRUE,
					'result' => $result
				);
				$this->session->set_userdata($field);
			}else{	
				$data['error'] = '<p>Email dan Password tidak ditemukan di dalam SISTEM.</p>';
			}
		}else{
			$data['error'] = validation_errors();
		}
		echo json_encode($data);
	}

	public function daftar(){
		$this->load->view('daftar');
	}

	public function daftar_action(){
		$this->form_validation->set_rules('nama','Nama Lengkap','required');
		$this->form_validation->set_rules('nik','NIK','required');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('tempat_lahir','Tempat Lahir','required');
		$this->form_validation->set_rules('tanggal_lahir','Tanggal Lahir','required');
		$this->form_validation->set_rules('email','Email','required');
		$this->form_validation->set_rules('jenis_kelamin','Jenis Kelamin','required');
		$data['success'] = false;
		if($this->form_validation->run() != false){
			$query = $this->Model_akun->addapi();
			if($query == true){
				$data['success'] = true;
				//$data['url'] = "/daftar";
			}
		}else{	
            $data['error'] = validation_errors();
		}
		echo json_encode($data);
	}

	public function add(){
		$data['list_jenispengaduan'] = $this->Model_jenispengaduan->getAll();
		$data['list_desa'] = $this->Model_desa->getAll();
		$this->load->view('add', $data);
	}

	// fungsi untuk meresize gambar
	function resizeImage($filename){
		$source_path = './media/' . $filename;
		$target_path = './media/thumbnail/';
		$config_manip = array(
			'image_library' => 'gd2',
			'source_image' => $source_path,
			'new_image' => $target_path,
			'maintain_ratio' => TRUE,
			'width' => 150,
			'height' => 150
		);

		$this->load->library('image_lib', $config_manip);
		if(!$this->image_lib->resize()){
			echo $this->image_lib->display_errors();
		}
		$this->image_lib->clear();
	}

	// fungsi untuk upload image
	function _uploadimage(){
		$config['upload_path'] = "./media";
		$config['allowed_types'] = "jpg|jpeg|png";
		$config['file_name'] = uniqid();
		$config['overwrite'] = true;
		$config['max_size'] = 3000;
		$this->load->library('upload', $config);
		if($this->upload->do_upload('image_file')){
			$this->resizeImage($this->upload->data('file_name'));
			return $this->upload->data('file_name');
		}
		return "default.png";
	}

	public function addaction(){
		$data['success'] = false;
		$this->form_validation->set_rules('jenispengaduan_id', 'Jenis Pengaduan', 'required');
		$user_data = $this->session->userdata('result');
		if($user_data){
			if($this->form_validation->run() != false){
				$file = $this->_uploadimage();
				$user_id = $user_data->id;
				$query = $this->Model_pengaduan->add($file, $user_id);
				
				if($query == true){
					$data['success'] = true;
				}
			}else{
				$data['error'] = validation_errors();
			}
		}
		echo json_encode($data);
	}
}