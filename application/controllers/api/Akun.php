<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {
	function __construct(){
		parent:: __construct();
		header('Access-Control-Allow-Origin: *');
    	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$this->load->model('Model_akun', 'model');
	}

	public function index(){
		$id = NULL;
		$msg['success'] = false;
		if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
		if($this->model->cek_api() == TRUE){
			$msg['success'] = true;
			$result = $this->model->getAll();
			if($result){
				$msg['success'] = true;
				$msg['status'] = 200;
				$msg['data'] = $result;
				if($id != NULL){
					$msg['data'] = $result[0];
				}
			}
		}
		echo json_encode($msg);
	}

	// public function add(){
	// 	$data['success'] = false;
	// 	if($this->model->cek_api() == TRUE){
	// 		$query = $this->model->add();
	// 		if($query == true){
	// 			$data['success'] = true;
	// 		}
	// 	}
	// 	echo json_encode($data);
	// }

	// public function edit(){
	// 	$data['success'] = false;
	// 	if($this->model->cek_api() == TRUE){
	// 		$query = $this->model->update();
	// 		if($query == true){
	// 			$data['success'] = true;
	// 		}
	// 	}
	// 	echo json_encode($data);
	// }

	// public function delete(){
	// 	$data['success'] = false;
	// 	if($this->model->cek_api() == TRUE){
	// 		$query = $this->model->delete();
	// 		if($query == true){
	// 			$data['success'] = true;
	// 		}
	// 	}
	// 	echo json_encode($data);
	// }

	public function login(){
		$msg['success'] = false;
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$this->db->where('nik', $username);
		$this->db->or_where('email', $username);
		$this->db->where('password', md5($password));
		$query = $this->db->get('akun');
		$msg['username'] = $username;
		$msg['password'] = $password;
		if($query->num_rows() > 0){
			$result = $query->result();
			$result = $result[0];			
			unset($result->password);
			$msg['success'] = true;			
			$msg['result'] = $result;
			// update data akun api_key 
			$generate_apikey = uniqid();
			$field['api_key'] = $generate_apikey;
			$this->db->where('id', $result->id);
			$this->db->update('akun', $field);
		}
		echo json_encode($msg);
	}

	public function daftar(){
		$data['success'] = false;
		$query = $this->model->addapi();
		if($query == true){
			$data['success'] = true;
		}
		echo json_encode($data);
	}
} 