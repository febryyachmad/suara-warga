<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desa extends CI_Controller {
	function __construct(){
		parent:: __construct();
		header('Access-Control-Allow-Origin: *');
    	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    	$this->load->model('Model_desa', 'model');
    	$this->load->model('Model_akun', 'model_akun');
	}

	public function index(){
		$id = NULL;
		$msg['success'] = false;
		if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
		if($this->model_akun->cek_api() == TRUE){
			$msg['success'] = true;
			$result = $this->model->getAll();
			if($result){
				$msg['success'] = true;
				$msg['status'] = 200;
				$msg['data'] = $result;
				if($id != NULL){
					$msg['data'] = $result[0];
				}
			}
		}
		echo json_encode($msg);
	}

	public function add(){
		$data['success'] = false;
		if($this->model_akun->cek_api() == TRUE){
			$query = $this->model->add();
			if($query == true){
				$data['success'] = true;
			}
		}
		echo json_encode($data);
	}

	public function edit(){
		$data['success'] = false;
		if($this->model_akun->cek_api() == TRUE){
			$query = $this->model->update();
			if($query == true){
				$data['success'] = true;
			}
		}
		echo json_encode($data);
	}

	public function delete(){
		$data['success'] = false;
		if($this->model_akun->cek_api() == TRUE){
			$query = $this->model->delete();
			if($query == true){
				$data['success'] = true;
			}
		}
		echo json_encode($data);
	}
}