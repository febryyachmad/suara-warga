<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Model_akun', 'model');
		$this->load->model('Model_grup', 'model_grup');
		$this->load->library('form_validation');
		if(!$this->session->userdata('logged_in')){
			redirect(base_url("admin/login"), "refresh");
		}
	}

	public function index(){
		$data['title'] = "Akun";
		$data['menu_active'] = "akun";
		$data['url'] = 'admin/akun/add';
		$this->load->view('admin/akun', $data);
	}

	public function add(){
		$data['title'] = "Akun";
		$data['menu_active'] = "akun";
		$data['list_grup'] = $this->model_grup->getAll();
		$data['action'] = 'admin/akun/add_action';
		$this->load->view('admin/akun_add', $data);
	}

	public function edit($id) {
		$data['title'] = "Akun";
		$data['menu_active'] = "akun";
		$data['list_grup'] = $this->model_grup->getAll();
		$data['_id'] = $id;
		$data['action'] = 'admin/akun/edit_action';
		$this->load->view('admin/akun_edit', $data);
	}


	public function edit_profil() {
		$akun = $this->session->userdata('result');
		$data['title'] = "Profil";
		$data['menu_active'] = "profil";
		$data['list_grup'] = $this->model_grup->getAll();
		$data['_id'] = $akun->id;
		$data['action'] = 'admin/akun/edit_action';
		$this->load->view('admin/edit_profil', $data);
	}

	public function editpassword($id) {
		$data['title'] = "Ganti Password";
		$data['menu_active'] = "akun";
		$data['_id'] = $id;
		$data['action'] = 'admin/akun/edit_password_action';
		$this->load->view('admin/akun_edit_password', $data);
	}

	function add_action(){
		// untuk validasi jika field tidak boleh kosong
		$this->form_validation->set_rules(
		        'nik', 'NIK',
		        'required|is_unique[akun.nik]',
		        array(
		                'required'      => 'You have not provided %s.',
		                'is_unique'     => 'This %s already exists.'
		        )
		);
		$this->form_validation->set_rules(
		        'email', 'Email',
		        'required|is_unique[akun.email]',
		        array(
		                'required'      => 'You have not provided %s.',
		                'is_unique'     => 'This %s already exists.'
		        )
		);
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('tempat_lahir','Tempat Lahir','required');
		$this->form_validation->set_rules('tanggal_lahir','Tanggal Lahir','required');
		$this->form_validation->set_rules('jenis_kelamin','Jenis Kelamin','required');
		$this->form_validation->set_rules('grup_id','Grup','required');
		$data['success'] = false;
		if($this->form_validation->run() != false){
			$query = $this->model->add();
			if($query == true){
				$data['success'] = true;
				$data['url'] = "admin/akun";
			}
		}else{	
            $data['error'] = validation_errors();
		}
		echo json_encode($data);
	}

	// untuk mengecheck email jika sama
	function check_user_email($email) {        
	    if($this->input->post('id'))
	        $id = $this->input->post('id');
	    else
	        $id = '';
	    $result = $this->model->check_unique_user_email($id, $email);
	    if($result == 0)
	        $response = true;
	    else {
	        $this->form_validation->set_message('check_user_email', 'Email must be unique');
	        $response = false;
	    }
	    return $response;
	}

	function check_user_nik($nik) {        
	    if($this->input->post('id'))
	        $id = $this->input->post('id');
	    else
	        $id = '';
	    $result = $this->model->check_unique_user_nik($id, $nik);
	    if($result == 0)
	        $response = true;
	    else {
	        $this->form_validation->set_message('check_user_nik', 'NIK must be unique');
	        $response = false;
	    }
	    return $response;
	}

	function edit_action(){
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_user_email');
		$this->form_validation->set_rules('nik', 'NIK', 'required|callback_check_user_nik');
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('tempat_lahir','Tempat Lahir','required');
		$this->form_validation->set_rules('tanggal_lahir','Tanggal Lahir','required');
		$this->form_validation->set_rules('jenis_kelamin','Jenis Kelamin','required');
		$this->form_validation->set_rules('email','Email','required');
		$this->form_validation->set_rules('grup_id','Grup','required');
		$data['success'] = false;
		if($this->form_validation->run() != false){
			$query = $this->model->update();
			if($query == true){
				$data['success'] = true;
				$data['url'] = "admin/akun";
			}
		}else{	
            $data['error'] = validation_errors();
		}
		echo json_encode($data);
	}

	function edit_password_action(){
		$data['success'] = false;
		$query = $this->model->update_password();
		if($query == true){
			$data['success'] = true;
			$data['url'] = "admin/akun";
		}
		
		echo json_encode($data);
	}

	function delete(){
		$result = $this->model->delete();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	public function show(){
		$limit = 20;
		$start = 0;
		$next = 20;
		$previous = 0;
		$q = "";
		$id = NULL;

		if(isset($_REQUEST['limit'])) $limit = $_REQUEST['limit'];
		if(isset($_REQUEST['start'])) $start = $_REQUEST['start'];
		if(isset($_REQUEST['q'])) $q = $_REQUEST['q'];
		if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];

		$result = $this->model->getlist($limit, $start, $q, $id);
		$total_count_page = count($result);

		$total_count = $this->model->get_count($q);
		$total_pagination = $total_count / $limit;
		$total_pagination = (int)$total_pagination;
		if ($total_count % $limit > 0){
			$total_pagination = $total_pagination + 1;
		}
		$link_next = "";
		if(count($result) == $limit){
			$link_next = "/admin/show?limit=".$limit."&start=".$next;
		}

		$link_prev = "";
		if($start != 0){
			$link_prev = "/admin/show?limit=".$limit."&start=".$previous;
		}

		if((int)$start != 0){
			$next = (int)$start+20;
			$previous = (int)$start-20;
		}
		$meta = array(
			'start' => $start,
			'limit' => $limit,
			'next' => $link_next,
			'previous' => $link_prev,
			'total_count' => $total_count,
			'total_pagination' => $total_pagination,
			'total_count_page' => $total_count_page,
		);

		$data = array(
			'meta' => $meta,
			'objects' => $result,
		);
		if(isset($_REQUEST['id'])){
			$data = $result[0];
		}
		echo json_encode($data);
	}

	function getall(){
		$data = $this->model->getAll();
		echo json_encode($data);
	}
}