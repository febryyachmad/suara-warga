<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desa extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Model_desa', 'model');
		$this->load->library('form_validation');
		if(!$this->session->userdata('logged_in')){
			redirect(base_url("admin/login"), "refresh");
		}
	}

	public function index(){
		$data['title'] = "Desa";
		$data['menu_active'] = "desa";
		$data['url'] = 'admin/desa/add';
		$this->load->view('admin/desa', $data);
	}

	public function add(){
		$data['title'] = "Desa";
		$data['menu_active'] = "desa";
		$data['action'] = 'admin/desa/add_action';
		$this->load->view('admin/desa_add', $data);
	}

	public function edit($id) {
		$data['title'] = "Desa";
		$data['menu_active'] = "desa";
		$data['_id'] = $id;
		$data['action'] = 'admin/desa/edit_action';
		$this->load->view('admin/desa_edit', $data);
	}

	function add_action(){
		// untuk validasi jika field tidak boleh kosong
		$this->form_validation->set_rules('nama_desa','Nama Desa','required');
		$data['success'] = false;
		if($this->form_validation->run() != false){
			$query = $this->model->add();
			if($query == true){
				$data['success'] = true;
				$data['url'] = "/admin/desa";
			}
		}else{	
            $data['error'] = validation_errors();
		}
		echo json_encode($data);
	}

	function edit_action(){
		$this->form_validation->set_rules('nama_desa','Nama Desa','required');
		$data['success'] = false;
		if($this->form_validation->run() != false){
			$query = $this->model->update();
			if($query == true){
				$data['success'] = true;
				$data['url'] = "/admin/desa";
			}
		}else{	
            $data['error'] = validation_errors();
		}
		echo json_encode($data);
	}

	function delete(){
		$result = $this->model->delete();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	public function show(){
		$limit = 20;
		$start = 0;
		$next = 20;
		$previous = 0;
		$q = "";
		$id = NULL;

		if(isset($_REQUEST['limit'])) $limit = $_REQUEST['limit'];
		if(isset($_REQUEST['start'])) $start = $_REQUEST['start'];
		if(isset($_REQUEST['q'])) $q = $_REQUEST['q'];
		if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];

		$result = $this->model->getlist($limit, $start, $q, $id);
		$total_count_page = count($result);

		$total_count = $this->model->get_count($q);
		$total_pagination = $total_count / $limit;
		$total_pagination = (int)$total_pagination;
		if ($total_count % $limit > 0){
			$total_pagination = $total_pagination + 1;
		}
		$link_next = "";
		if(count($result) == $limit){
			$link_next = "/admin/show?limit=".$limit."&start=".$next;
		}

		$link_prev = "";
		if($start != 0){
			$link_prev = "/admin/show?limit=".$limit."&start=".$previous;
		}

		if((int)$start != 0){
			$next = (int)$start+20;
			$previous = (int)$start-20;
		}
		$meta = array(
			'start' => $start,
			'limit' => $limit,
			'next' => $link_next,
			'previous' => $link_prev,
			'total_count' => $total_count,
			'total_pagination' => $total_pagination,
			'total_count_page' => $total_count_page,
		);

		$data = array(
			'meta' => $meta,
			'objects' => $result,
		);
		if(isset($_REQUEST['id'])){
			$data = $result[0];
		}
		echo json_encode($data);
	}

	function getall(){
		$data = $this->model->getAll();
		echo json_encode($data);
	}
}