<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik extends CI_Controller {
	function __construct(){
		parent:: __construct();
		if(!$this->session->userdata('logged_in')){
			redirect(base_url("admin/login"), "refresh");
		}
	}

	public function index(){
		$data['title'] = "Statistik";
		$data['menu_active'] = "statistik";
		$this->db->from('pengaduan');
		$this->db->where('status_id', 1);
		$data['jumlah_pengaduan_belum'] = $this->db->count_all_results();
		$this->db->from('pengaduan');
		$this->db->where('status_id', 2);
		$data['jumlah_pengaduan_proses'] = $this->db->count_all_results();
		$this->db->from('pengaduan');
		$this->db->where('status_id', 3);
		$data['jumlah_pengaduan_selesai'] = $this->db->count_all_results();
		$this->load->view('admin/statistik', $data);
	}
}