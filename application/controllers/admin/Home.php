<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent:: __construct();
		if(!$this->session->userdata('logged_in')){
			redirect(base_url("admin/login"), "refresh");
		}
	}

	public function getgrup(){
		$query_grup = NULL;
		$userku = $this->session->userdata('result'); // ambil data user session
		if(isset($userku))
			if($userku->grup_id)
				if($userku->grup_id != 8)
					if($userku->grup_id == 2){
						// admin sarana prasarana
						$query_grup = $this->db->where('jenispengaduan_id', 8);
					}elseif($userku->grup_id == 3){
						//insfrastuktur
						$query_grup = $this->db->where('jenispengaduan_id', 6);
					}elseif($userku->grup_id == 6){
						//pelayanan
						$query_grup = $this->db->where('jenispengaduan_id', 9);
					}elseif($userku->grup_id == 5){
						//kesehatan
						$query_grup = $this->db->where('jenispengaduan_id', 3);
					}
		return $query_grup;

	}

	public function jumlah_pengaduan(){
		$this->db->from('pengaduan');
		$this->getgrup();
		$this->db->where('status_id', 1);
		return $this->db->count_all_results();
	}

	public function index(){
		$data['title'] = "Selamat Datang";
		$data['menu_active'] = "home";
		// query jumlah semua pengaduan
		
		// query jumlah pengaduan yang belum tertangani
		
		$this->db->or_where('status_id', 2);

		$data['jumlah_pengaduan_belum'] = $this->db->count_all_results();
		// query jumlah pengaduan yang sudah tertangani
		$this->db->from('pengaduan');
		$this->db->where('status_id', 3);
		$this->getgrup();
		$data['jumlah_pengaduan_sudah'] = $this->db->count_all_results();
		// query jumlah pengaduan yang di archive kan
		$this->db->from('pengaduan');
		$this->db->where('status_id', 4);
		$this->getgrup();
		$data['jumlah_pengaduan_archive'] = $this->db->count_all_results();
		$this->load->view('admin/home', $data);
	}
}